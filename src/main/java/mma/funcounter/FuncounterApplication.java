package mma.funcounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuncounterApplication {

    public static void main(String[] args) {
        SpringApplication.run(FuncounterApplication.class, args);
    }

}
