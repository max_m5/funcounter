package mma.funcounter.controllers;

import mma.funcounter.models.Counter;
import mma.funcounter.services.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1.0/counters")
public class CounterController {

    private final CounterService counterService;

    @Autowired
    public CounterController(CounterService counterService) {
        this.counterService = counterService;
    }

    @GetMapping("/create")
    public Counter createCounter(@RequestParam String name) {
        return counterService.addCounter(name);
    }

    @GetMapping("/increment")
    public Counter incrementCounter(@RequestParam String name) {
        return  counterService.incrementCounter(name);
    }

    @GetMapping("/value")
    public Counter getCounterValue(@RequestParam String name) {
        return counterService.getCounterValue(name);
    }

    @GetMapping("/delete")
    public Counter deleteCounter(@RequestParam String name) {
        return counterService.deleteCounter(name);
    }

    @GetMapping("/sumall")
    public Integer getCountersSum() {
        return counterService.sumAllCounters();
    }

    @GetMapping("/list")
    public List<Counter> getAllCounters() {
        return counterService.getAllCountersNames();
    }
}
