package mma.funcounter.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Counter name already exists.")
public class CounterAlreadyExistsException extends RuntimeException{

    public CounterAlreadyExistsException() {
    }

    public CounterAlreadyExistsException(String message) {
        super(message);
    }

    public CounterAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CounterAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public CounterAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
