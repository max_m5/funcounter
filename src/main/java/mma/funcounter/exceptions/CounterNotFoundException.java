package mma.funcounter.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Counter name not found.")
public class CounterNotFoundException extends RuntimeException {

    public CounterNotFoundException() {
    }

    public CounterNotFoundException(String message) {
        super(message);
    }

    public CounterNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CounterNotFoundException(Throwable cause) {
        super(cause);
    }

    public CounterNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
