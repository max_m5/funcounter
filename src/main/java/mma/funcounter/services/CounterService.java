package mma.funcounter.services;

import mma.funcounter.exceptions.CounterAlreadyExistsException;
import mma.funcounter.exceptions.CounterNotFoundException;
import mma.funcounter.models.Counter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CounterService {

    private Map<String, Integer> counters = new HashMap<>();

    public Counter addCounter(String name) {
        if (!counters.containsKey(name)) {
            counters.put(name, 0);
            return new Counter(name, 0);
        } else {
            throw new CounterAlreadyExistsException();
        }
    }

    public Counter incrementCounter(String name) {
        if (counters.containsKey(name)) {
            Integer current = counters.get(name);
            counters.put(name, current + 1);
            return new Counter(name, current + 1);
        } else {
            throw new CounterNotFoundException();
        }
    }

    public Counter getCounterValue(String name) {
        return new Counter(name, counters.get(name));
    }

    public Counter deleteCounter(String name) {
        if (!counters.containsKey(name)) {
            throw new CounterNotFoundException();
        }
        Counter returned = new Counter(name, counters.get(name));
        counters.remove(name);
        return returned;
    }

    public Integer sumAllCounters() {
        return counters.values().stream().mapToInt(Integer::intValue).sum();
    }

    public List<Counter> getAllCountersNames() {
        List<Counter> res = new ArrayList<>();
        for (Map.Entry<String, Integer> counter : counters.entrySet()) {
            res.add(new Counter(counter.getKey(), counter.getValue()));
        }
        return res;
    }
}
